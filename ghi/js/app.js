function createCard(name, description, pictureUrl, startDateStr, endDateStr,location) {
    return `
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${startDateStr} - ${endDateStr}
      </div>
    `;
  }
  function createPlaceholder(num) {
    return `
      <div class="card" id="card-${num}" style="box-shadow:0px 5px 5px 5px #00000030">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-6"></span>
          <span class="placeholder col-8"></span>
        </p>
        <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
      </div>
    </div>
    <p></p>
    `;
  }

function raiseAlarm(message){
    return `
    <div class="alert alert-warning" role="alert">
        ${message}
    </div>
    `
}
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try{
        const response = await fetch(url);
        if(!response.ok){
            const main = document.querySelector('main');
            main.innerHTML = raiseAlarm("Invalid response.") + main.innerHTML;

        }else{
            const data = await response.json();

            const columns = document.querySelectorAll('.col');
            let columnnum = 0;


            const placeholders = []
            for(let i = 0; i<data.conferences.length; i++){
                placeholders.push(createPlaceholder(i));
            }
            for(let placeholder in placeholders){
                columns[columnnum].innerHTML += placeholders[placeholder];
                columnnum += 1;
                if(columnnum==columns.length){
                    columnnum=0;
                }
            }

            let placenum = 0;
            columnnum = 0
            for(let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const endDate = new Date(details.conference.ends);
                    const startDateStr = startDate.getDate() + "/" + startDate.getMonth() + "/" + startDate.getFullYear();
                    const endDateStr = endDate.getDate() + "/" + endDate.getMonth() + "/" + endDate.getFullYear();
                    const location = details.conference.location.name;
                    const html = createCard(title,description,pictureUrl, startDateStr,endDateStr, location);
                    // columns[columnnum].innerHTML += html;
                    // columnnum++;
                    // if(columnnum==columns.length){
                    //     columnnum=0;
                    // }
                    // console.log("here?")
                    document.getElementById(`card-${placenum}`).innerHTML = html;

                    // console.log("or here...")
                    placenum++;
                }
            };

        }
    }
    catch(e){
        const main = document.querySelector('main');
        main.innerHTML = raiseAlarm(`${e.name} occured.`) + main.innerHTML;
    }


});
