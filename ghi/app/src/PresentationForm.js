import React, { useEffect, useState } from 'react';

function LocationForm() {
    const [presName, setPresName] = useState('');
    const [presEmail, setPresEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynposis] = useState('');
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('');

    const handlePresNameChange = (event) => {
        const value = event.target.value;
        setPresName(value);
    }
    const handlePresEmailChange = (event) => {
        const value = event.target.value;
        setPresEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynposisChange = (event) => {
        const value = event.target.value;
        setSynposis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setConferences(
                data.conferences
            )
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.presenter_name = presName;
        data.presenter_email = presEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference

        console.log(data);
        const locationUrl = `http://localhost:8000${data.conference}presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setPresName('');
            setPresEmail('');
            setCompanyName('');
            setTitle('');
            setSynposis('');
            setConference('');
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Presenter name" onChange={handlePresNameChange} required type="text" name="presenter_name" value={presName} id="presenter_name"
                                className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Presenter email" onChange={handlePresEmailChange} required type="text" name="presenter_email" value={presEmail} id="presenter_email"
                                className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Company name" onChange={handleCompanyNameChange} required type="text" name="company_name" value={companyName} id="company_name"
                                className="form-control" />
                            <label htmlFor="company_name">Conpany name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Title" required onChange={handleTitleChange} type="text" name="title" id="title" value={title} className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Synopsis">Synopsis</label>
                            <textarea placeholder="Synopsis"onChange={handleSynposisChange} required type="text" id="synopsis" value={synopsis} name="synopsis"
                                className="form-control">
                            </textarea>
                        </div>
                        <div className="mb-3">
                            <select required name="conference" onChange={handleConferenceChange} value={conference} id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    )
                                })};
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;
